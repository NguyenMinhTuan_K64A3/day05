<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<style>
  .container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: auto;
    margin-top: 2rem;
  }

  .form-group {
    width: 30vw;
    height: inherit;
    background-color: white;
    padding: 2rem;
    /* margin-top: 2rem; */
    display: flex;
    flex-direction: column;
    border: 2px solid #4f85b4;
  }

  .form-child {
    height: 36px;
    margin: 10px 0px;
    display: flex;
    align-items: center;
    justify-content: space-around;
    /* margin: 1.4rem 0px; */
  }

  .form-child-text {
    background-color: #70ad46;
    height: inherit;
    width: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0px 6px;
    border: 2px solid #4f85b4;
    color: white;
  }

  .form-input-text {
    height: inherit;
    width: 200px;
    padding: 0px;
    display: flex;
    align-items: center;
    /* border: 2px solid #4f85b4; */
  }

  .form-input-image {
    /* height: inherit; */
    width: 200px;
    padding: 0px;
  }

  .form-input-radio {
    display: flex;
    align-items: center;
    height: inherit;
    width: 200px;
    padding: 0px;
  }

  .form-input-select {
    display: flex;
    align-items: center;
    height: inherit;
    width: 200px;
    padding: 0px
  }

  #hello {
    border: 2px solid #4f85b4;
    height: inherit;
  }

  .form-input-birthday {
    display: flex;
    align-items: center;
    height: inherit;
    width: 160px;
    margin-right: 40px;
    padding: 0px 0px 0px 10px;
    box-sizing: border-box;
    border: 2px solid #4f85b4;
  }

  .form-child-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 2rem;
  }

  .btn-submit {
    height: 44px;
    width: 140px;
    border-radius: 5px;
    border: 2px solid #4f85b4;
    background-color: #70ad46;
    color: white;
  }
</style>

<body>
  <?php
  session_start();
  $fullName = isset($_SESSION['data']['fullName']) ? $_SESSION['data']['fullName'] : "";
  $gender = isset($_SESSION['data']['gender']) ? $_SESSION['data']['gender'] : "";
  $khoa = isset($_SESSION['data']['khoa']) ? $_SESSION['data']['khoa'] : "";
  $birthday = isset($_SESSION['data']['birthday']) ? $_SESSION['data']['birthday'] : "";
  $address = isset($_SESSION['data']['address']) ? $_SESSION['data']['address'] : "";
  $image = isset($_SESSION['image']) ? $_SESSION['image'] : "";
  ?>
  <div class="container">
    <div class="form-group">
      <div class="form-child">
        <label class="form-child-text">
          Họ và tên
        </label>
        <div class="form-input-text">
          <?php
          echo $fullName;
          ?>
        </div>
      </div>

      <div class="form-child">
        <label class="form-child-text">
          Giới tính
        </label>
        <div class="form-input-text">
          <?php
          echo $gender;
          ?>
        </div>
      </div>

      <div class="form-child">
        <label class="form-child-text">
          Phân khoa
        </label>
        <div class="form-input-text">
          <?php
          $khoaArray = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
          foreach ($khoaArray as $key => $value) {
            if ($key == $khoa) {
              echo $value;
            }
          }
          ?>
          </select>
        </div>
      </div>

      <div class="form-child" date-date-format="dd/MM/yyyy">
        <label class="form-child-text">
          Ngày sinh
        </label>
        <div class="form-input-text">
          <?php
          echo $birthday;
          ?>
          </select>
        </div>
      </div>

      <div class="form-child">
        <label class="form-child-text">
          Địa chỉ
        </label>
        <div class="form-input-text">
          <?php
          echo $address;
          ?>
        </div>
      </div>

      <div class="form-child">
        <label class="form-child-text">
          Hình ảnh
        </label>
        <div class="form-input-text">
          <?php
          echo '<span class="form-control1"><img src="' . $image . '" height="40px" width="40px"></span>'
          ?>
        </div>
      </div>
      <div class="form-child-btn">
        <input type="button" name="btnSubmit" id="btnSubmit" class="btn-submit" value="Xác nhận">
      </div>

    </div>
  </div>
</body>

</html>